namespace filipek.pl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReplyReportcol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "ReplyReport", c => c.String());
            AlterColumn("dbo.Reports", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Reports", "Type", c => c.String(nullable: false));
            AlterColumn("dbo.Reports", "Category", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reports", "Category", c => c.String());
            AlterColumn("dbo.Reports", "Type", c => c.String());
            AlterColumn("dbo.Reports", "Description", c => c.String());
            DropColumn("dbo.Reports", "ReplyReport");
        }
    }
}
