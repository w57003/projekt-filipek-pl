namespace filipek.pl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Report_Db : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Type = c.String(),
                        Category = c.String(),
                        Data_of_issue = c.DateTime(nullable: false),
                        Status = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reports", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Reports", new[] { "User_Id" });
            DropTable("dbo.Reports");
        }
    }
}
