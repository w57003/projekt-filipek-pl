﻿
var request = new XMLHttpRequest()

document.getElementById('searchLocation').onclick = function () {
    var location = document.getElementById('getLocation').value;
    console.log(location);
    request.open('GET', 'http://api.openweathermap.org/data/2.5/weather?q=' + location + '&appid=5018a726f091f2f233b3af11f9e207c8&units=metric', true)
    request.onload = function () {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response)

        if (request.status >= 200 && request.status < 400) {
            var icon = data.weather[0].icon;
            var src = 'http://openweathermap.org/img/w/' + icon + '.png';
            var img = document.createElement('img');
            img.src = src;
            nameLocation
            document.getElementById('iconhtml').innerHTML = '<img src=' + src + ' class="weather-img"/>';
            document.getElementById('nameLocation').innerHTML = data.name;
            document.getElementById('country').innerHTML = data.sys.country;
            document.getElementById('weather').innerHTML = data.weather[0].main;
            document.getElementById('wind').innerHTML = data.wind.speed + ' m/s';
            document.getElementById('tempLow').innerHTML = data.main.temp_min + ' <sup>o</sup>C';
            document.getElementById('tempMax').innerHTML = data.main.temp_max + ' <sup>o</sup>C';
            document.getElementById('humidity').innerHTML = data.main.humidity + ' %';

        } else {
            console.log('error')
        }
    }
    request.send()
}

var request = new XMLHttpRequest()
request.open('GET', 'http://api.nbp.pl/api/cenyzlota/?format=json', true)
request.onload = function () {
    var data = JSON.parse(this.response)

    if (request.status >= 200 && request.status < 400) {
        data.forEach(zloto => {
            document.getElementById('zloto').innerHTML = (zloto.cena)
        })
    } else {
        console.log('error')
    }
}
request.send()

var request = new XMLHttpRequest()
request.open('GET', 'http://api.nbp.pl/api/exchangerates/tables/A/?format=json', true)
request.onload = function () {
    var data = JSON.parse(this.response)

    if (request.status >= 200 && request.status < 400) {
        data.forEach(rate => {
            document.getElementById('usd').innerHTML = (rate.rates[1].mid)
            document.getElementById('eur').innerHTML = (rate.rates[7].mid)
            document.getElementById('gbp').innerHTML = (rate.rates[10].mid)
            document.getElementById('chf').innerHTML = (rate.rates[9].mid)
            document.getElementById('nok').innerHTML = (rate.rates[16].mid)
        })
    } else {
        console.log('error')
    }
}
request.send()

