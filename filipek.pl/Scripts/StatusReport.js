﻿var el = document.getElementById('StatusReport');

function CheckStatus() {
    if (el.innerHTML == 'Rozpatrzono') {

        el.className = "badge p-2 badge-success";
    }
    else if (el.innerHTML == 'Wymaga dodatkowych informacji') {
        el.className = "badge p-2 badge-info";
    }
    else if (el.innerHTML == 'Odrzucono') {
        el.className = "badge p-2 badge-danger";
    }
    else if (el.innerHTML == 'Oczekuje') {
        el.className = "badge p-2 badge-secondary";
    }
}
CheckStatus();
