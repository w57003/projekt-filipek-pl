﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace filipek.pl.Models
{
    public class Report
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Category { get; set; }
        public DateTime Data_of_issue { get; set; }
        public string Status { get; set; }
        public string ReplyReport { get; set; }
        public ApplicationUser User { get; set; }
    }
}