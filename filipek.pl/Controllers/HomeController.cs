﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace filipek.pl.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Carrer()
        {
            return View();
        }
        public ActionResult Employees()
        {
            return View();
        }
        public ActionResult News()
        {
            return View();
        }
        public ActionResult Tips()
        {
            return View();
        }
        public ActionResult UsefulSoftware()
        {
            return View();
        }
        public ActionResult Offer()
        {
            return View();
        }
        public ActionResult Regulations()
        {
            return View();
        }
        public ActionResult Price_List()
        {
            return View();
        }
    }
}