﻿using filipek.pl.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(filipek.pl.Startup))]
namespace filipek.pl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesAndUsers();
        }
        private void CreateRolesAndUsers()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "admin@filipek.pl";
                user.Email = "admin@filipek.pl";
                string password = "P@$$w0rd";

                var chkWorker = UserManager.Create(user, password);

                if (chkWorker.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Admin");
                }
            }

            if (!roleManager.RoleExists("Worker"))
            {
                var role = new IdentityRole();
                role.Name = "Worker";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "worker@filipek.pl";
                user.Email = "worker@filipek.pl";
                string password = "P@$$w0rd";

                var chkWorker = UserManager.Create(user, password);

                if (chkWorker.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Worker");
                }
            }

            if (!roleManager.RoleExists("User"))
            {
                var role = new IdentityRole();
                role.Name = "User";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "user@filipek.pl";
                user.Email = "user@filipek.pl";
                string password = "P@$$w0rd";

                var chkWorker = UserManager.Create(user, password);

                if (chkWorker.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "User");
                }

            }
        }
    }
}
